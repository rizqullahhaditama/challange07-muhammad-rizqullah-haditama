import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Card, Col, Row } from "react-bootstrap";
import { getAllCars } from "../../../redux/actions/carAction";

function CarContent3() {
    const { getAllCarsResult, getAllCarsLoading, getAllCarsError } =
        useSelector((state) => state.carReducer);
    const dispatch = useDispatch();

    useEffect(() => {
        //panggil dari actions
        console.log("use effect component did mount");
        dispatch(getAllCars());
    }, [dispatch]);

    return (
        <div className="container">
            <Row>
                {/* opsi berhasil */}
                {getAllCarsResult ? (
                    getAllCarsResult.map((item) => {
                        return (
                            <Col className="mb-3" lg="4" key={item.id}>
                                <Card
                                    className=" h-100 "
                                    style={{ width: "25rem" }}
                                >
                                    <Card.Img
                                        src={item.image}
                                        style={{
                                            height: "20rem",
                                            width: "100%",
                                            objectFit: "cover",
                                        }}
                                    />
                                    <Card.Body>
                                        <Card.Text>
                                            <h6>
                                                {item.manufacture}/{item.model}
                                            </h6>
                                        </Card.Text>

                                        <h4 className="harga">
                                            {" "}
                                            Rp {item.rentPerDay} / hari{" "}
                                        </h4>
                                        <Card.Text>
                                            {item.description}
                                        </Card.Text>
                                        <h6>
                                            <i className="bi bi-people me-2"></i>
                                            {item.capacity} orang
                                        </h6>
                                        <h6>
                                            <i className="bi bi-gear me-2"></i>
                                            {item.transmission}
                                        </h6>
                                        <h6>
                                            <i className="bi bi-calendar me-2"></i>
                                            Tahun {item.year}
                                        </h6>
                                        <Card.Body className="d-flex flex-column">
                                            <button className="btn btn-success fw-bold ">
                                                Pilih Mobil
                                            </button>
                                        </Card.Body>
                                    </Card.Body>
                                </Card>
                            </Col>
                        );
                    })
                ) : // opsi kalo loading
                getAllCarsLoading ? (
                    <p>Loading ...</p>
                ) : (
                    // opsi kalo error dan false
                    <p>{getAllCarsError ? getAllCarsError : "Data Kosong"}</p>
                )}
            </Row>
        </div>
    );
}

export default CarContent3;
