import "./CariMobil.css";
import { Card, Row, Col } from "react-bootstrap";

const CarFilter = ({ cars }) => {
  return (
    <div className="container justify-content-center">
      <Row>
        {cars.map((car) => (
          <Col className="mb-3" lg="4" key={car.id}>
            <Card className=" h-100 " style={{ width: "20rem" }}>
              <Card.Img src={car.image} style={{ height: "20rem" }} />
              <Card.Body>
                <Card.Text>
                  <h6>
                    {car.manufacture}/{car.model}
                  </h6>
                </Card.Text>

                <h4 className="harga"> Rp {car.rentPerDay} / hari </h4>
                <Card.Text>{car.description}</Card.Text>
                <h6>
                  <i className="bi bi-people me-2"></i>
                  {car.capacity} orang
                </h6>
                <h6>
                  <i className="bi bi-gear me-2"></i>
                  {car.transmission}
                </h6>
                <h6>
                  <i className="bi bi-calendar me-2"></i>Tahun {car.year}
                </h6>
                <Card.Body className="d-flex flex-column">
                  <button className="btn btn-success fw-bold ">
                    Pilih Mobil
                  </button>
                </Card.Body>
              </Card.Body>
            </Card>
          </Col>
        ))}
      </Row>
    </div>
  );
};

export default CarFilter;
